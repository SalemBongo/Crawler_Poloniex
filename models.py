# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_poloniex
# Description:   contains a table model.
# Version:       1.7.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

from sqlalchemy import Column, Integer, String, Float, TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base


class Data(declarative_base()):  # create model of table
    __tablename__ = 'data_table'

    id = Column(Integer, primary_key=True)
    ticker = Column(String, nullable=False)
    price = Column(Float, nullable=False)
    timestamp = Column(TIMESTAMP, nullable=False)

    def __init__(self, ticker, price, timestamp):
        self.ticker = ticker
        self.price = price
        self.timestamp = timestamp

    def __repr__(self):
        return '{} : {} - {} - {})'.format(
            self.__tablename__, self.ticker, self.price, self.timestamp)
