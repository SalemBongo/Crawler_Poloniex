# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_poloniex
# Description:   creates a database and a table for importing data into it.
# Version:       1.7.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from sqlalchemy import (create_engine, MetaData, Table, Column, Integer,
                        String, Float, TIMESTAMP)

from config import log


# create database
engine_a = psycopg2.connect(dbname='postgres',
                            host='localhost', port='5432',
                            user='admin', password='123')
engine_a.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cursor_a = engine_a.cursor()
db = 'mydb'


try:
    cursor_a.execute("CREATE DATABASE {}".format(db))
except Exception as e:
    log.error(e)


# create table
engine_b = create_engine("postgresql+psycopg2://admin:123@localhost:5432/mydb")
metadata = MetaData()
data_table = Table('data_table', metadata,
                   Column('id', Integer, primary_key=True),
                   Column('ticker', String, nullable=False),
                   Column('price', Float, nullable=False),
                   Column('timestamp', TIMESTAMP, unique=True))


try:
    metadata.create_all(engine_b)
except Exception as e:
    log.error(e)
