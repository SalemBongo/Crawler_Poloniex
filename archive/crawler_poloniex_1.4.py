# ---------------------------------------------------------------------------------------------------------------------
# Project:       Crawler_Poloniex
# Description:   Test
# Version:       1.4 | Python 3.6
# Date:          02.05.2018
# Author:        Stanislav Fux | https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session
import requests
import json
import time

engine = create_engine("postgresql+psycopg2://admin:123@localhost:5432/my_db")  # соединение с БД
metadata = MetaData()  # создаем модель таблицы
data_table = Table('data', metadata,
                   Column('id', Integer, primary_key=True),
                   Column('ticker', String),
                   Column('price', Float),
                   Column('timestamp', String)
                   )
metadata.create_all(engine)  # создаем таблицу

Base = declarative_base()


class Data(Base):  # создаем класс таблицы
    __tablename__ = 'data'

    id = Column(Integer, primary_key=True)
    ticker = Column(String)
    price = Column(Float)
    timestamp = Column(String)

    def __init__(self, ticker, price, timestamp):
        self.ticker = ticker
        self.price = price
        self.timestamp = timestamp

    def __repr__(self):
        return "<Data('%s','%s', '%s')>" % (self.ticker, self.price, self.timestamp)


while True:  # запускаем цикл-сбор данных
    req = requests.get('https://poloniex.com/public?command=returnTicker')  # получаем данные с ресурса
    obj = json.loads(req.text)  # конвертируем данные

    obj_a = obj['USDT_BTC']['last']
    obj_b = obj['USDT_ETH']['last']
    obj_c = obj['BTC_ETH']['last']

    t = time.asctime()

    session = Session(bind=engine)
    signal_a = Data(ticker='USDT-BTC', price=obj_a, timestamp=t)
    signal_b = Data(ticker='USDT-ETH', price=obj_b, timestamp=t)
    signal_c = Data(ticker='BTC-ETH', price=obj_c, timestamp=t)
    session.add_all([signal_a, signal_b, signal_c])  # добавление данных
    session.commit()  # подтверэжение
    print('Done')
    time.sleep(10)
else:
    print('Oops! :(')
