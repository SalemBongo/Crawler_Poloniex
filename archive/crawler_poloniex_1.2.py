# ---------------------------------------------------------------------------------------------------------------------
# Project:       Crawler_Poloniex
# Description:   Test
# Version:       1.2 | Python 3.6
# Date:          28.04.2018
# Author:        Stanislav Fux | https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import requests
import json
import time
from sqlalchemy import MetaData, create_engine
from sqlalchemy import Table, Column, Integer, String, Numeric

engine = create_engine("postgresql+psycopg2://admin:123@localhost:5432/my_db")  # соединение с БД

metadata = MetaData()  # создаем модель таблицы
data_table = Table('data', metadata,
                   Column('id', Integer),
                   Column('ticker', String),
                   Column('price', Numeric),
                   Column('timestamp', Numeric)
                   )
metadata.create_all(engine)

req = requests.get('https://poloniex.com/public?command=returnTicker')  # получаем данные с площадки
obj = json.loads(req.text)  # конвертируем данные

data = [obj['USDT_BTC']['last'],
        obj['USDT_ETH']['last'],
        obj['BTC_ETH']['last']]
t = time.asctime()

time.sleep(60)
