# ---------------------------------------------------------------------------------------------------------------------
# Project:       Crawler_Poloniex
# Description:   Test
# Version:       1.3 | Python 3.6
# Date:          01.05.2018
# Author:        Stanislav Fux | https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy import Column, Integer, String, Float
import requests
import json
import time

engine = create_engine("postgresql+psycopg2://admin:123@localhost:5432/my_db")  # соединение с БД
metadata = MetaData()  # создаем модель таблицы
data_table = Table('data', metadata,
                   Column('id', Integer, primary_key=True),
                   Column('ticker', String),
                   Column('price', Float),
                   Column('timestamp', String)
                   )
metadata.create_all(engine)  # создаем таблицу

con = engine.connect()  # поключаемся к БД и вставляем строки
con.execute(data_table.insert(),
            ticker='USDT-BTC')
con.execute(data_table.insert(),
            ticker='USDT-ETH')
con.execute(data_table.insert(),
            ticker='BTC-ETH')

while True:  # запускаем беспокнечный цикл, обновляем содержимое строк
    req = requests.get('https://poloniex.com/public?command=returnTicker')  # получаем данные с ресурса
    obj = json.loads(req.text)  # конвертируем данные

    t = time.asctime()
    obj_a = obj['USDT_BTC']['last']
    obj_b = obj['USDT_ETH']['last']
    obj_c = obj['BTC_ETH']['last']

    con.execute(data_table.update
                (data_table.c.id == 1),
                price=obj_a, timestamp=t)
    con.execute(data_table.update
                (data_table.c.id == 2),
                price=obj_b, timestamp=t)
    con.execute(data_table.update
                (data_table.c.id == 3),
                price=obj_c, timestamp=t)
    print('Done')
    time.sleep(60)
else:
    print('Oops! :(')
