# ---------------------------------------------------------------------------------------------------------------------
# Project:       Crawler_Poloniex
# Description:   Test
# Version:       1.1 | Python 3.6
# Date:          26.04.2018
# Author:        Stanislav Fux | https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import requests
import json
import time

while True:  # бесконечный цикл
    r = requests.get('https://poloniex.com/public?command=returnTicker')  # получаем данные с площадки
    obj = json.loads(r.text)  # конвертируем данные
    print(
        "BTC \ USD :",
        obj['USDT_BTC']['last'], '\n'
                                 "ETH \ USD :",
        obj['USDT_ETH']['last'], '\n'
                                 "BTC \ ETH :",
        obj['BTC_ETH']['last']
    )
    print('Timestamp : ' + time.asctime())
    time.sleep(60)
else:
    print('Oops! Connection error. :(')
